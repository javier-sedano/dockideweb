This is the source code for https://javier-sedano.gitlab.io/dockideweb/.

Instructions:
* Suggested environment:
  * Visual Studio Code. Plugins:
    * Docker
    * Remote - Containers
  * Docker. Docker Desktop for Windows is tested
* Clone from git
* Open folder in VSCode
  * Remote container will be detected, and offered to "Reopen in container". Do so.
  * If cloned through SSH, first time doing git push, pull,... it will fail with wrong known-host. To solve it, do the first `git fetch` on terminal inside the container, verify and accept the host, and from then on, git on VSCode will work.
* _tools/initDependencies.sh
* _tools/serve.sh
* Browse http://localhost:4000

Useful links:
* https://jekyllrb.com/
* https://rubygems.org/
