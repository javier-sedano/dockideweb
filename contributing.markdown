---
title: Contributing
layout: default
permalink: /contributing/
---

Contributions welcome. Just fork the project, create a Merge Request and we will evaluate it. Previous discussion in the [forum](https://gitlab.com/javier-sedano/dockide/-/issues) is encouraged.

Development principles:

* SOLID, Clean code, DRY. Hexagonal architecture.
* DevOps, CI, CD, CQ.
* TDD, BDD.
* Branch-based development with Merge Request (expect constructive criticism).

[Source code](https://gitlab.com/javier-sedano/dockide).

[Source code of this website](https://gitlab.com/javier-sedano/dockideweb).

