#!/bin/bash -e
pushd `dirname $0`/.. > /dev/null
DOWNLOADS=downloads
ISFIRST="YES"
cd $DOWNLOADS
for VERSION in `ls -r`
do
  echo ""
  echo "Version $VERSION"
  echo ""
  cd $VERSION
  for FLAVOUR in *
  do
    echo -n "* $FLAVOUR:"
    cd $FLAVOUR
    for FILE in *
    do
      echo -n " [$FILE]({{ \"$DOWNLOADS/$VERSION/$FLAVOUR/$FILE\" | relative_url }})"
    done
    echo ""
    cd ..
  done
  cd ..

  if [ "$ISFIRST" ]
  then
    echo ""
    echo "<details markdown=\"1\"><summary>Old versions</summary>"
    ISFIRST=""
  fi
done

  if [ ! "$ISFIRST" ]
  then
    echo ""
    echo "</details>"
  fi

cd ..
popd > /dev/null
