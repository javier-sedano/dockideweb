#!/bin/bash -e
pushd `dirname $0`/..
_tools/create_downloads_list.sh > downloads_list.markdown
bundle exec jekyll serve --host 0.0.0.0 --verbose --incremental
popd
