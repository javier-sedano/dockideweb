---
title: Home
layout: default
permalink: /
---

Dockide allows to run your full development environment (IDE, tools, auxiliary services,...) inside Docker Compose. It is heavily inspired by [Visual Studio Code Remote Container development](https://code.visualstudio.com/docs/remote/containers), but trying to be able to run any IDE (VSCode, IntelliJ, Emacs, Atom,...).

Work in progress in very early alpha stage.

Ask for help in the [forum](https://gitlab.com/javier-sedano/dockide/-/issues).

Licensed under [GPLv3](https://gitlab.com/javier-sedano/dockide/-/raw/master/COPYING.txt).
