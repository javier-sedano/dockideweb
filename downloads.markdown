---
title: Downloads
layout: default
permalink: /downloads/
---

Different flavours are provided:

* Windows: standalone Windows binary
* Windows_slim: Windows binary without the .Net Core runtime; download .Net Core 3.1 Runtime from [Microsoft](https://dotnet.microsoft.com/download/dotnet-core/current/runtime).
* Linux: standalone Linux binary
* Linux_slim: Linux binary without the .Net Core runtime; download .Net Core 3.1 Runtime from [Microsoft](https://dotnet.microsoft.com/download/dotnet-core/current/runtime).

{% include_relative downloads_list.markdown %}

NOTE: this is very early alpha, so expect versions <1.0.0 to be removed without prior warnning.
